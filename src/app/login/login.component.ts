import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('usernameInput', { static: true }) usernameInput: ElementRef;
  @ViewChild('passwordInput', { static: true }) passwordInput: ElementRef;


  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  login() {
    let username = this.usernameInput.nativeElement.value;
    let password = this.passwordInput.nativeElement.value;
    this.authService.isValid(username, password);
  }

}
