import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public username: string;

  constructor(public userService: UserService, private authService: AuthService) { }

  ngOnInit(): void {
    this.username = this.authService.activeUser

    this.userService.getUserAccounts(this.username);
      this.userService.getUserFarms(this.username);
  }

}
