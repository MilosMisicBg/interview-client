import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string = "http://interview-env.eba-i6sb7wmu.us-east-2.elasticbeanstalk.com/users/validate"

  public activeUser: string;

  constructor(private httpClient: HttpClient, private router: Router) { }

  isValid(username: string, password: string) {

    const headers = {
      'Authorization': 'Basic ' + btoa('user:password'),
      'Access-Control-Allow-Origin': '*'
    }
    const body = { username: username, password: password }
    this.httpClient.post<any>(this.url, body, { headers }).subscribe(data => {
      if (data) {
        this.activeUser = username;
        this.router.navigate(['/dashboard']);
      }
    })
  }

}
