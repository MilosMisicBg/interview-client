import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public accounts: Object;
  public farms: Object;

  url: string = "http://interview-env.eba-i6sb7wmu.us-east-2.elasticbeanstalk.com/users/";

  constructor(private httpClient: HttpClient) { }

  public getUserAccounts(username: string) {
    let tempUrl = this.url + "accounts" + "/" + username;
    this.httpClient.get(tempUrl, {
      headers: new HttpHeaders({
        'Authorization': 'Basic ' + btoa('user:password'),
        'Access-Control-Allow-Origin': '*'
      })
    }).toPromise().then((response) => {
      this.accounts = response;
    });
  }

  public getUserFarms(username: string) {
    let tempUrl = this.url + "farms" + "/" + username;
    this.httpClient.get(tempUrl, {
      headers: new HttpHeaders({
        'Authorization': 'Basic ' + btoa('user:password'),
        'Access-Control-Allow-Origin': '*'
      })
    }).toPromise().then((response) => {
      this.farms = response;
    });
  }
}
